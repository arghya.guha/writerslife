import React, { Component } from 'react';

class App extends Component {
  render() {
    return (
      <div className="text-center">
        <div className="bg-blue-800 m-6 p-6 rounded shadow-lg">
          <h2 className="text-white text-2xl font-mono">Writerslife</h2>
        </div>
        <p className="text-base">
          Welcome to your writing pad. This is your writing life companion.
        </p>
      </div>
    );
  }
}

export default App;
