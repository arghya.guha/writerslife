# Writerslife

Online writing and distibuting platform for aspiring writers

## Build

`npm run build`

## Run in dev mode

`npm run dev`

[![pipeline status](https://gitlab.com/arghya.guha/writerslife/badges/master/pipeline.svg)](https://gitlab.com/arghya.guha/writerslife/commits/master)
